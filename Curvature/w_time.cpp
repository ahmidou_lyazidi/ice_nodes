#include "w_time.h"

	double CStopWatch::LIToSecs( LARGE_INTEGER & L) {
return ((double)L.QuadPart /(double)frequency.QuadPart) ;
}

CStopWatch::CStopWatch(){
	timer.start.QuadPart=0;
	timer.stop.QuadPart=0;
	QueryPerformanceFrequency( &frequency ) ;
}

void CStopWatch::startTimer( ) {
	QueryPerformanceCounter(&timer.start) ;
}

void CStopWatch::stopTimer(XSI::CString message ) {
	QueryPerformanceCounter(&timer.stop) ;
	LARGE_INTEGER time;
	time.QuadPart = timer.stop.QuadPart - timer.start.QuadPart;
	XSI::Application().LogMessage(message+XSI::CString(LIToSecs( time)));
}
